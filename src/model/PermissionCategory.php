<?php


namespace Peter\Permissions\Model;


use Peter\Permissions\Exceptions\Exception;
use Peter\Permissions\Validate\PermissionCategoryVerify;
use think\Db;


class PermissionCategory extends Base
{
    /**
     * 编辑权限分组
     * @param array $data
     * @return $this
     * @throws Exception
     */
    public function saveCategory($data = [])
    {
        if (!empty($data)) {
            $this->data($data);
        }
        $validate = new PermissionCategoryVerify();
        if (!$validate->check($this)) {
            throw new Exception($validate->getError());
        }
        $data = $this->getData();
        if (isset($data['id']) && !empty($data['id'])) {
            $this->isUpdate(true);
        }
        $this->save();
        return $this;
    }

    /**
     * 删除权限分组
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function delCategory($id)
    {
        $where = [];
        if (is_array($id)) {
            $where[] = ['id', 'IN', $id];
        } else {
            $id = (int)$id;
            if (is_numeric($id) && $id > 0) {
                $where[] = ['id', '=', $id];
            } else {
                throw new Exception('删除条件错误');
            }
        }

        if ($this->where($where)->delete() === false) {
            throw new Exception('删除权限分组出错');
        }
        return true;
    }
    /**
     * @param $where
     * @return mixed
     * 获取权限分组
     */
    public function getCategory($where)
    {
        $model = Db::name('permission_category')->setConnection($this->getConnection());
        if (is_numeric($where)) {
            return $model->where('id', $where)->find();
        } else {
            return $model->where($where)->select();
        }
    }
}